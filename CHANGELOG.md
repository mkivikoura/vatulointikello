# 1.0.3 (2018-04-25)

* Fractions are not displayed in cost anymore
* Cost displayed in french locale (thousands separated by space)

# 1.0.2 (2018-04-19)

* Made responsive; page can be viewed on smaller (mobile) screens properly.

# 1.0.1 (2018-04-16)

* Add footer with copyright notice, license and link to repository.

# 1.0.0 (2018-04-16)

* Official release.
