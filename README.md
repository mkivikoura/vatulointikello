# Vatulointikello

*(Probably best translation 'slacking clock')*

Web app to track money spent on inefficient use of time in meetings.

[Live Demo of Master Branch](http://vatulointikello.kivikoura.fi)

## Project uses

 * [Bootstrap v4](https://getbootstrap.com) front-end component library
 * [Vue](https://vuejs.org) Javascript framework
