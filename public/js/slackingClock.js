var app = new Vue({
    el: '#app',
    data: {
        displayUpdatePeriod: 1,

        sumOfPartics: 1,
        avgHourlyCost: 0,

        timerClassStates: [
            'fa-hourglass-start',
            'fa-hourglass-half',
            'fa-hourglass-end'
        ],
        timerCurrentClassIndex: 2,

        timePassed: 0,
        timerLimit: 3600,
        interval: null
    },
    methods: {
        toggleTimer: function() {
            if (!this.interval) {
                this.interval = setInterval(this.timer, 1000)
                this.timerCurrentClassIndex = 0
            } else {
                this.stopTimer()
            }
        },
        stopTimer: function() {
            clearInterval(this.interval);
            this.interval = null
            this.timerCurrentClassIndex = 2
            this.timerLimit = 3600
        },
        timer: function() {
            if (this.timerLimit <= 0) {
                this.stopTimer()
            }

            this.timerLimit -= 1
            this.timePassed += 1

            if (this.timerCurrentClassIndex == 2) {
                this.timerCurrentClassIndex = 0
            }
            else {
                this.timerCurrentClassIndex += 1
            }
        }
    },
    computed: {
        costPerSecond: function() {
            return ( this.avgHourlyCost / 3600 ) * this.sumOfPartics
        },
        displayCost: function() {
            raw_num = Math.floor(this.timePassed
                / this.displayUpdatePeriod)
                * this.costPerSecond
                * this.displayUpdatePeriod;
            return raw_num.toLocaleString('fr-FR',{maximumFractionDigits: 0})
        }
    }
})
